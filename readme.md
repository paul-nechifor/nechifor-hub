# Nechifor Hub

A box I use for coordinating web tasks.

## Usage

    vagrant up local

## Live Usage

You have to have your 64 char Digital Ocean token in `private/token`.

    vagrant up nechifor-hub

## License

MIT
